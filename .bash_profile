# load rbenv
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

# load bashrc
if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

# git completion
if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi
